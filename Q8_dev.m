clear
clc
close all

X = [0,0,0;
    1,0,0;
    1.5,1,1;
    0.5,1,1];
E = [1,2;2,3;3,4;4,1];
XC = [X;(X(E(:,2),:)+X(E(:,1),:))/2];

scale = 0.1;
Xr = (rand(4,3)*2-1)*scale
XC(5:end,:) = XC(5:end,:)+Xr;
EE = [1,5;5,2;2,6;6,3;3,7;7,4;4,8;8,1];
k = [1,5,2,6,3,7,4,8,1];
xfigure;
% plot3(XC(k,1), XC(k,2), XC(k,3), 'k-o'); hold on;
% text(XC(:,1), XC(:,2), cellstr( num2str([1:8]') ), 'BackgroundColor','w')
axis([-0.5,2,-0.5,2]); axis equal; view(3)

iele = Q8Element(XC,'gorder',3)
h = iele.vizQ8Ele('Refinements',10, 'Light', true, 'FaceColor','c')
axis equal

