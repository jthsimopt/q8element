classdef Q8Element
    %Q8 element class 
    %   O = Q8(XC)
    %   O = Q8(XC,parameters)
    %   parameters:
    %   'domain'   - Parametric domain. Can be [0,1] or [-1,1]. Default is
    %   [0,1].
    %   'gorder'   - Gaussorder. Default is 3 (Full).
    %
    %   4----7----3
    %   |         |
    %   8         6
    %   |         |
    %   1----5----2
    %
    properties
        P
        ReferenceDomain
        GaussOrder
        Area
        BaseFunction
        GaussPoints
        nP
    end
    
    methods
        
        %Constructor
        function O = Q8Element(XC,varargin)
            
            p = inputParser;
            p.addParameter('domain',[0,1])
            p.addParameter('gorder',3)
            p.parse(varargin{:});
            V = p.Results;
            domain = V.domain;
            gorder = V.gorder;
            
            %% Gauss Points
            [ GP, GW ] = QuadIntegration(gorder, domain);
            nGP = size(GP,1);
            %% Isoparametric mapping
            [fi, dfidr, dfids, rsArea] = Q8ParametricMap(GP,domain);
            
            %%
            dim = size(XC,2);
            if dim == 2
                %% 2D
                xc = XC(:,1); yc = XC(:,2);
                dxdr = dfidr*xc;
                dydr = dfidr*yc;
                dxds = dfids*xc;
                dyds = dfids*yc;
                
                J = zeros(2,2,nGP);
                detJ = zeros(nGP,1);
                area = zeros(nGP,1);
                dfidx = zeros(nGP,8);
                dfidy = dfidx;
                for i = 1:nGP
                    Ji = [dxdr(i), dydr(i);
                        dxds(i), dyds(i)];
                    J(:,:,i) = Ji;
                    
                    dfidri = dfidr(i,:);
                    dfidsi = dfids(i,:);
                    dfidxyi = Ji\[dfidri;
                        dfidsi];
                    dfidx(i,:) = dfidxyi(1,:);
                    dfidy(i,:) = dfidxyi(2,:);
                    
                    detJ(i) = det(Ji);
                    area(i) = abs(detJ(i))*rsArea;
                end
                
                
                
                
                baseFcn.fi = fi;
                baseFcn.dfidx = dfidx;
                baseFcn.dfidy = dfidy;
                baseFcn.detJ = detJ;
                baseFcn.dfidy = dfidy;
                baseFcn.area = area;
                baseFcn.J = J;
                
                GPx = [fi*xc,fi*yc];
                
            else 
                %% 3D
                xc = XC(:,1); yc = XC(:,2); zc = XC(:,3);
                dxdr = dfidr*xc;
                dydr = dfidr*yc;
                dzdr = dfidr*zc;
                
                dxds = dfids*xc;
                dyds = dfids*yc;
                dzds = dfids*zc;
                
                dXdr = [dxdr, dydr, dzdr];
                dXds = [dxds, dyds, dzds];
                n1 = cross(dXdr,dXds,2);
                n = n1./(n1(:,1).^2+n1(:,2).^2+n1(:,3).^2).^0.5;
                
%                 xg = fi*xc; yg = fi*yc; zg = fi*zc;
%                 quiver3(xg,yg,zg,n(:,1),n(:,2),n(:,3),0.4)
                
                J = zeros(3,3,nGP);
                detJ = zeros(nGP,1);
                area = zeros(nGP,1);
                dfidx = zeros(nGP,8);
                dfidy = dfidx;
                dfidz = dfidx;
                for i = 1:nGP
                    Ji = [dXdr(i,:);
                          dXds(i,:);
                          n(i,:)];
                    J(:,:,i) = Ji;
                    
                    dfidri = dfidr(i,:);
                    dfidsi = dfids(i,:);
                    dfidti = zeros(1,8);
                    dfidxyzi = Ji\[dfidri;
                                  dfidsi;
                                  dfidti];
                    dfidx(i,:) = dfidxyzi(1,:);
                    dfidy(i,:) = dfidxyzi(2,:);
                    dfidz(i,:) = dfidxyzi(3,:);
                    
                    detJ(i) = det(Ji);
                    area(i) = abs(detJ(i))*rsArea;
                end
                
                baseFcn.fi = fi;
                baseFcn.dfidx = dfidx;
                baseFcn.dfidy = dfidy;
                baseFcn.dfidz = dfidz;
                baseFcn.detJ = detJ;
                baseFcn.area = area;
                baseFcn.n = n;
                baseFcn.J = J;
                
                GPx = [fi*xc,fi*yc,fi*zc];
                
                
            end
            
            
            %%
            O.P = XC;
            O.GaussOrder = gorder;
            O.ReferenceDomain = domain;
            O.BaseFunction = baseFcn;
            O.Area = sum(area.*GW);
            O.GaussPoints = GPx;
            O.nP = size(GPx,1);
            
        end
        
        function h = vizQ8Ele(O, varargin)
            % h = vizQ8Ele()
            % h = vizQ8Ele(Refinements)
            % h = vizQ8Ele(Refinements, parameters)
            %
            % parameters:
            % Refinements      - Number of refinements, default: 0. Set to -1
            % to display as triangulated.
            % 'FaceColor'      - Default: 'w'
            % 'FaceLighting'   - Default: 'gouraud'
            % 'EdgeColor'      - Default: 'k'
            % 'InnerEdgeColor' - Default: [1,1,1]*0.4
            % 'EdgeLineWidth'  - Default: 1.5
            % 'Light'          - Default: false
            
            %% Input Parsin
            p = inputParser;
            addOptional(p,'Refinements',0);
            addParameter(p,'FaceColor','w');
            addParameter(p,'FaceLighting','gouraud');
            addParameter(p,'EdgeColor','k');
            addParameter(p,'InnerEdgeColor',[1,1,1]*0.4);
            addParameter(p,'EdgeLineWidth',1.5);
            addParameter(p,'InnerEdgeAlpha',1);
            addParameter(p,'Light',false);
            parse(p,varargin{:});
            PR = p.Results;
            
            %% Parameters
            XC = O.P;
            Q8nodes = [1,2,3,4,5,6,7,8];
            nQ8Ele = size(Q8nodes,1);
            
            %% Discretizing
            np = PR.Refinements+2;
            nEle = nQ8Ele*np^2;
            
            dx=1/np;
            [X,Y] = meshgrid(0:dx:1,0:dx:1);
            x=X(:);y=Y(:);
            nnods = length(x);
            
            nodes = zeros(np*np,4);
            M = reshape(1:nnods,np+1,np+1);
            c = 1;
            for j = 1:np
                for i = 1:np
                    ii = i:i+1;
                    jj = j:j+1;
                    ee = M(ii,jj);
                    nodes(c,:) = ee([1,3,4,2])';
                    c = c+1;
                end
            end
%             x = [0,1,1,0]'; y = [0,0,1,1]';
%             nodes = [1,2,3,4];
%             ref = np-1;
%             for i = 1:ref
%                 [nodes, x, y] = refine(nodes, x, y);
%             end
%             xy = [x,y];
            tt = nodes;
%             xfigure;
%             patch('Faces',tt,'Vertices',[x,y],'FaceColor','w'); view(2)
%             text(x,y,cellstr( num2str( (1:nnods)' ) ), 'BackgroundColor','w')
%             
            %% Mapping
            [fi, ~, ~, ~] = Q8ParametricMap([x,y],[0,1]);
            
            %% Interpolating
            if np > 1
                k = convhull(x,y);
                nk = length(k);

                T = zeros(nEle,4);
                X = zeros(nQ8Ele*nnods,3);
                kk = zeros(nQ8Ele,nk);

                for iel=1:nQ8Ele
                    iQn = Q8nodes(iel,:);
                    xc=XC(iQn,1); yc=XC(iQn,2); zc=XC(iQn,3);

                    xn=fi*xc;
                    yn=fi*yc;
                    zn=fi*zc;

                    % Insert coordinates from iel into X, that will contain all coordinates
                    lx=size(xn,1);
                    if iel==1
                        X(iel:lx,:)=[xn,yn,zn];
                    else
                        X((iel-1)*lx+1:iel*lx,:)=[xn,yn,zn];
                    end

                    % Global triangle matrix is assembled
                    lt=size(tt,1);
                    if iel==1
                        T(iel:lt,:)=tt;
                        tt0=tt;
                        tt1=tt0;
                    else
                        maxtt=max(tt(:));
                        tt1=tt1+maxtt;
                        lt=size(tt1,1);
                        T((iel-1)*lt+1:iel*lt,:)=tt1;
                    end

                    % 
                    k1 = k'+max(kk(:));
                    kk(iel,:) = k1;

                end
            else
                T = Q8nodes(:,1:4);
                X = XC;
            end
            
            %% Draw Graphics
            holdstate = ishold;
            h.patch =  patch('faces',T,'vertices',X,'FaceColor', PR.FaceColor);
            hold on
            set(h.patch,'FaceLighting', PR.FaceLighting)
            if PR.Light
                hca = gca;
                lightexist = 0;
                for i = 1:length(hca.Children)
                    if isa(hca.Children(i), 'matlab.graphics.primitive.Light')
                        lightexist = 1;
                    end
                end
                if ~lightexist
                    h.light = light;
                end
            end            
            if np > 1
                h.patch.EdgeColor = PR.InnerEdgeColor;
                h.edge=trimesh(kk,X(:,1),X(:,2),X(:,3));
                set(h.edge,'FaceColor','none')
                set(h.edge,'EdgeColor', PR.EdgeColor)
                set(h.edge,'EdgeLighting','none','LineWidth', PR.EdgeLineWidth)
                h.patch.EdgeAlpha = PR.InnerEdgeAlpha;
            else
                h.patch.EdgeColor = PR.EdgeColor;
                h.patch.LineWidth = PR.EdgeLineWidth;
            end
            
            if ~holdstate
                hold off
            end
            
            
        end
    end
    
end

function [fi, dfidr, dfids, rsArea] = Q8ParametricMap(X,domain)
    x = X(:,1); y = X(:,2);
    if isequal(domain,[-1,1])
        fi = [-(x - 1).*(y - 1).*(2*x + 2*y - 1),...
            x.*(y - 1).*(2*y - 2*x + 1),...
            x.*y.*(2*x + 2*y - 3),...
            y.*(x - 1).*(2*x - 2*y + 1),...
            4*x.*(x - 1).*(y - 1),...
            -4*x.*y.*(y - 1),...
            -4*x.*y.*(x - 1),...
            4*y.*(x - 1).*(y - 1)];
        
        dfidr = [-(y - 1).*(4*x + 2*y - 3),...
            (y - 1).*(2*y - 4*x + 1),...
            y.*(4*x + 2*y - 3),...
            -y.*(2*y - 4*x + 1),...
            4*(2*x - 1).*(y - 1),...
            -4*y.*(y - 1),...
            -4*y.*(2*x - 1),...
            4*y.*(y - 1)];
        
        dfids = [-(x - 1).*(2*x + 4*y - 3),...
            -x.*(2*x - 4*y + 1),...
            x.*(2*x + 4*y - 3),...
            (x - 1).*(2*x - 4*y + 1),...
            4*x.*(x - 1),...
            -4*x.*(2*y - 1),...
            -4*x.*(x - 1),...
            4*(2*y - 1).*(x - 1)];
        rsArea = 4;
    else %[0,1]
        fi = [-(x - 1).*(y - 1).*(2*x + 2*y - 1),...
            x.*(y - 1).*(2*y - 2*x + 1),...
            x.*y.*(2*x + 2*y - 3),...
            y.*(x - 1).*(2*x - 2*y + 1),...
            4*x.*(x - 1).*(y - 1),...
            -4*x.*y.*(y - 1),...
            -4*x.*y.*(x - 1),...
            4*y.*(x - 1).*(y - 1)];
        dfidr = [-(y - 1).*(4*x + 2*y - 3),...
            (y - 1).*(2*y - 4*x + 1),...
            y.*(4*x + 2*y - 3),...
            -y.*(2*y - 4*x + 1),...
            4*(2*x - 1).*(y - 1),...
            -4*y.*(y - 1),...
            -4*y.*(2*x - 1),...
            4*y.*(y - 1)];
        dfids = [-(x - 1).*(2*x + 4*y - 3),...
            -x.*(2*x - 4*y + 1),...
            x.*(2*x + 4*y - 3),...
            (x - 1).*(2*x - 4*y + 1),...
            4*x.*(x - 1),...
            -4*x.*(2*y - 1),...
            -4*x.*(x - 1),...
            4*(2*y - 1).*(x - 1) ];
        rsArea = 1;
    end
end

function [ GP, GW ] = QuadIntegration(order, domain)
%QuadIntegration Produces Gauss integration points for quadliteral elements
%   [ GP, GW ] = QuadIntegration(order, domain)
%   order is Gauss Integration order
%   domain can either be [0,1] or [-1,1]
    
    if isequal(domain,[-1,1])
        [r, w, ~] = gauss(order,-1,1);
    elseif isequal(domain,[0,1])
        [r, w, ~] = gauss(order,0,1);
    else
       error('Domain can either be [0,1] or [-1,1]') 
    end
    [gx,gy] = meshgrid(r,r);
    [gwx,gwy] = meshgrid(w,w);
    GP = [gx(:),gy(:)];
    GW = [gwx(:).*gwy(:)];

end

function [x, w, A] = gauss(n, a, b)
    % 3-term recurrence coefficients:
    n = 1:(n - 1);
    beta = 1 ./ sqrt(4 - 1 ./ (n .* n));
    % Jacobi matrix:
    J = diag(beta, 1) + diag(beta, -1);
    % Eigenvalue decomposition:
    % e-values are used for abscissas, whereas e-vectors determine weights.
    [V, D] = eig(J);
    x = diag(D);
    % Size of domain:
    A = b - a;
    % Change of interval:
    %
    % Initally we have I0 = [ -1, 1 ]; now one gets I = [ a, b ] instead.
    %
    % The abscissas are Legendre points.
    %
    if ~(a == -1 && b == 1)
        x = 0.5 * A * x + 0.5 * (b + a);
    end
    % Weigths:
    w = V(1, :) .* V(1, :);
end

function [nodesn, xnodn, ynodn] = refine(nodes, xnod, ynod)

%==============================================================================
% refine.m
%==============================================================================
%
% Purpose:
%
% Remeshes a quadrilateral net to divide each element into four equal ones. The
% elements are assumed to be bilinear.
%
%
%
% Syntax:
%
% [nodesn, xnodn, ynodn] = refine(nodes, xnod, ynod);
%
%
%
% Input:
%
% nodes     integer    Each row represents an element and column entries corre-
%                      spond to node numbers on that element.
%
% xnod      double     Nodal x-coordinates.
% ynod      dobule     Nodal y-coordinates.
%
%
%
% Output:
%
% nodesn    integer    Remeshed counterpart of 'nodes'.
% xnodn     double     Remeshed counterpart of 'xnod'.
% ynodn     double     Remeshed counterpart of 'ynod'.
%
%============================= Have a lot of fun! =============================



% ------------------------------------------------------------- Initializations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% The dimensions of 'xnew' and 'ynew' usually are set too large:
%
%   length(xnew) = {maximum number of added nodes} = 5 * nele,
%
% is an overestimate of the number of added nodes in the refined mesh. 
%
% The reason why is to allocate space enough to avoid dynamic memory handling.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    nele = size(nodes, 1);                                      % # elements.
    nno = length(xnod);                                          % # nodes.
    nold = nno;                                                  % # initial nodes.
    xnew = zeros(5 * nele, 1);
    ynew = xnew;
    nodesn = zeros(4 * nele, 4);
    check = sparse([], [], [], nold, nold, 5 * nno);
    xnod=xnod(:);ynod=ynod(:);



    % ----------------------------------------------------------------- Preparation

    % All midpoint coordinates:

    xmid = 0.25 * sum(xnod(nodes), 2);
    ymid = 0.25 * sum(ynod(nodes), 2);
    if(nele==1)
        xmid = 0.25 * sum(xnod(nodes)', 2);
        ymid = 0.25 * sum(ynod(nodes)', 2);
    end   

    % Sort pairs of nodes (by number):

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % Done to reduce time needed for accessing 'check', a matrix used to keep track
    % of added nodes.
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    np1 = sort(nodes(:, [1 2]), 2);
    np2 = sort(nodes(:, [2 3]), 2);
    np3 = sort(nodes(:, [3 4]), 2);
    np4 = sort(nodes(:, [4 1]), 2);



    % ---------------------------------------------------------------- Element loop

    for iel = 1:nele

      % Fetch nodes on present element:
      iv = nodes(iel, :);

      % Fetch nodal pairs:
      p1 = np1(iel, :);
      p2 = np2(iel, :);
      p3 = np3(iel, :);
      p4 = np4(iel, :);


      % -----------------------------------------------------------------  Midpoint

      % Adjust nno (one node will be added):
      nno = nno + 1;

      % Add midpoint node (always occur):
      xnew(nno - nold) = xmid(iel);
      ynew(nno - nold) = ymid(iel); 

      % Numbering of central node:
      nodmid = nno;


      % --------------------------------------------- Side along 1:st and 2:nd node

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      %
      % Is there a node between them? If "no" enter if-statement and add one.
      %
      % There could be a node present already, following the refinement of an adja-
      % cent element, in which case nothing is to be done.
      %
      % We employ sorted pairs of nodes (p1-p4), using only half of the elements in
      % 'check' (namely the upper diagonal part), which saves time and space.
      %
      % Variables n1-n4 store check-values for faster access when setting 'nnodes'.
      %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      % Check if node is present:
      n1 = check(p1(1), p1(2));

      if (n1 == 0)

        % Add node: 
        nno = nno + 1;
        n1 = nno;

        % Coordinates for new node:
        xnew(nno - nold) = 0.5 * (xnod(iv(1)) + xnod(iv(2)));
        ynew(nno - nold) = 0.5 * (ynod(iv(1)) + ynod(iv(2)));

        % Store added node:
        check(p1(1), p1(2)) = n1;

      end 


      % --------------------------------------------- Side along 2:nd and 3:rd node

      n2 = check(p2(1), p2(2));

      if (n2 == 0)
        nno = nno + 1;
        n2 = nno;
        xnew(nno - nold) = 0.5 * (xnod(iv(2)) + xnod(iv(3)));
        ynew(nno - nold) = 0.5 * (ynod(iv(2)) + ynod(iv(3)));
        check(p2(1), p2(2)) = n2;
      end


      % --------------------------------------------- Side along 3:rd and 4:th node

      n3 = check(p3(1), p3(2));

      if (n3 == 0)
        nno = nno + 1;
        n3 = nno;
        xnew(nno - nold) = 0.5 * (xnod(iv(3)) + xnod(iv(4)));
        ynew(nno - nold) = 0.5 * (ynod(iv(3)) + ynod(iv(4)));
        check(p3(1), p3(2)) = n3;
      end


      % --------------------------------------------- Side along 1:st and 4:th node

      n4 = check(p4(1), p4(2));

      if (n4 == 0)
        nno = nno + 1;
        n4 = nno;
        xnew(nno - nold) = 0.5 * (xnod(iv(1)) + xnod(iv(4)));
        ynew(nno - nold) = 0.5 * (ynod(iv(1)) + ynod(iv(4)));
        check(p4(1), p4(2)) = n4;
      end


      % ----------------------------------------------------------- Update topology

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      %
      % Structure:
      %
      % Nodes on an element are given in counter-clockwise order, starting from the
      % lower-left corner. Original nodes keep their original numbers.
      %
      % Elements are numbered rowwise, also from the lower-left. They are, however,
      % all renumbered (in contrast to original nodes).
      %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      % Index variable:
      ind = 4 * iel;

      % Set nodes on the (k - 3):th to k:th element:
      nodesn((ind - 3):ind, :) = [ iv(1),   n1,      nodmid,  n4; 
                                   n1,      iv(2),   n2,      nodmid;
                                   nodmid,  n2,      iv(3),   n3;
                                   n4,      nodmid,  n3,      iv(4)   ];
    end


    % Discard superfluous nodal data:
    xnodn = [xnod; xnew(1:(nno - nold))];
    ynodn = [ynod; ynew(1:(nno - nold))];
end
